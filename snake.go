package main

import (
	"fmt"
	"log"
	"math/rand"
	"strings"
	"time"

	"github.com/mattn/go-tty"
)

const (
	none  = 0
	left  = 1
	down  = 2
	right = -1
	up    = -2
)

type SnakeGame struct {
	snakeCoords Queue[Coordinate]
	apple       Coordinate
	size        int
	snakeLength int
	points      int
	wait        float64
}

type Coordinate struct {
	x int
	y int
}

var stopGame = false
var moveQueue Queue[int]

func getCommands(tty *tty.TTY) {
	for !stopGame {
		r, err := tty.ReadRune()
		if err != nil {
			log.Fatal(err)
			stopGame = true
		}
		cmd := strings.ToLower(string(r))
		switch cmd {
		case "w":
			moveQueue.Enqueue(up)
		case "a":
			moveQueue.Enqueue(left)
		case "s":
			moveQueue.Enqueue(down)
		case "d":
			moveQueue.Enqueue(right)
		}
	}
}

func snakeHasCrashed(game SnakeGame) bool {
	snakeHead := game.snakeCoords.queue[len(game.snakeCoords.queue)-1]
	if snakeHead.x < 0 || snakeHead.y < 0 || snakeHead.x >= game.size || snakeHead.y >= game.size {
		return true
	}
	dedupMap := make(map[Coordinate]bool)
	for _, coord := range game.snakeCoords.queue {
		if dedupMap[coord] {
			return true
		}
		dedupMap[coord] = true
	}
	return false
}

func updateGame(game SnakeGame, newDirection *int) SnakeGame {
	snakeHead := game.snakeCoords.queue[len(game.snakeCoords.queue)-1]

	if snakeHasCrashed(game) {
		stopGame = true
		return game
	}

	var newSnakeHead Coordinate
	switch *newDirection {
	case up:
		newSnakeHead = Coordinate{snakeHead.x, snakeHead.y - 1}
	case down:
		newSnakeHead = Coordinate{snakeHead.x, snakeHead.y + 1}
	case left:
		newSnakeHead = Coordinate{snakeHead.x - 1, snakeHead.y}
	case right:
		newSnakeHead = Coordinate{snakeHead.x + 1, snakeHead.y}
	}
	game.snakeCoords.Enqueue(newSnakeHead)

	if len(game.snakeCoords.queue) > game.snakeLength {
		game.snakeCoords.Dequeue()
	}

	if newSnakeHead == game.apple {
		game.points += 10
		game.snakeLength += 2
		game.wait = game.wait * 0.95
		game.apple = randomCoordinate(game.size)
	}

	return game
}

func contains(s []Coordinate, e Coordinate) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func printGame(game SnakeGame) {

	if game.points > 100 {
		colorCode := 31 + rand.Intn(6)
		fmt.Print("\033[" + fmt.Sprint(colorCode) + "m")
	}

	fmt.Println(strings.Repeat(" ", game.size-3) + "SNAKE" + "     Score: " + fmt.Sprint(game.points))

	fmt.Println(strings.Join(strings.Split(strings.Repeat("#", game.size+2), ""), " "))
	for i := 0; i < game.size; i++ {
		line := "#"
		for j := 0; j < game.size; j++ {
			if contains(game.snakeCoords.queue, Coordinate{j, i}) {
				line += " @"
			} else if (game.apple == Coordinate{j, i}) {
				line += " Ó"
			} else {
				line += "  "
			}
		}
		line += " #"
		fmt.Println((line))
	}
	fmt.Println(strings.Join(strings.Split(strings.Repeat("#", game.size+2), ""), " "))
	fmt.Println()
}

func randomCoordinate(size int) Coordinate {
	return Coordinate{x: rand.Intn(size), y: rand.Intn(size)}
}

func main() {

	tty, err := tty.Open()
	if err != nil {
		fmt.Println(err)
	}
	defer tty.Close()
	var snakeCoords Queue[Coordinate]

	game := SnakeGame{
		snakeCoords: snakeCoords,
		apple:       randomCoordinate(25),
		size:        25,
		snakeLength: 3,
		points:      0,
		wait:        200,
	}
	game.snakeCoords.Enqueue(Coordinate{x: game.size / 2, y: game.size / 2})
	currentDirection := up

	go getCommands(tty)

	for !stopGame {
		newDirection := moveQueue.Dequeue()
		if newDirection == nil {
			newDirection = &currentDirection
		}
		if currentDirection*-1 == *newDirection {
			newDirection = &currentDirection
		}
		currentDirection = *newDirection
		game = updateGame(game, newDirection)
		printGame(game)
		time.Sleep(time.Duration(game.wait) * time.Millisecond)
	}

	for i := 0; i < game.size/2; i++ {
		fmt.Println()
	}

	fmt.Print("\033[31m")

	fmt.Println(strings.Repeat(" ", game.size-4) + "GAME OVER")

	scoreString := fmt.Sprint(game.points)
	if len(scoreString)%2 == 1 {
		scoreString = " " + scoreString
	}

	fmt.Println(strings.Repeat(" ", game.size-((7+len(scoreString))/2)) + "Score: " + scoreString)

	for i := 0; i < game.size/2; i++ {
		fmt.Println()
	}

}
