package main

type Queue[T any] struct {
	queue []T
}

func (q *Queue[T]) Enqueue(x T) {
	q.queue = append(q.queue, x)
}

func (q *Queue[T]) Dequeue() *T {
	if len(q.queue) == 0 {
		return nil
	}
	ret := q.queue[0]
	q.queue = q.queue[1:]
	return &ret
}
