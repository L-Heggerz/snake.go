package main

import (
	"fmt"
	"testing"
)

func assertEquals[T comparable](a T, b T, t *testing.T) {
	if a != b {
		t.Fatalf(fmt.Sprintf("FAILURE: %v != %v", a, b))
	}
}

func assertNil[T any](a *T, t *testing.T) {
	if a != nil {
		t.Fatalf(fmt.Sprintf("FAILURE: %v != nil", a))
	}
}

func TestCanQueueThenDeQueue(t *testing.T) {
	var testQueue Queue[int]
	testQueue.Enqueue(420)
	assertEquals(420, *testQueue.Dequeue(), t)
}

func TestReturnsNilIfEmpty(t *testing.T) {
	var testQueue Queue[int]
	assertNil(testQueue.Dequeue(), t)
}

func TestQueueMaintainsOrder(t *testing.T) {
	var testQueue Queue[string]
	testQueue.Enqueue("a")
	testQueue.Enqueue("b")
	testQueue.Enqueue("c")
	assertEquals("a", *testQueue.Dequeue(), t)
	assertEquals("b", *testQueue.Dequeue(), t)
	assertEquals("c", *testQueue.Dequeue(), t)
	assertNil(testQueue.Dequeue(), t)
}
