module snake

go 1.19

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/mattn/go-isatty v0.0.10 // indirect
	github.com/mattn/go-tty v0.0.4 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20191120155948-bd437916bb0e // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
